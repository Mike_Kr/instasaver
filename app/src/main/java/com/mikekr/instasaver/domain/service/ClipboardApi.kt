package com.mikekr.instasaver.domain.service

interface ClipboardApi {
    var serviceIsRunning: Boolean
    fun handleService()
    fun isClipboardServiceRunning(serviceClass: Class<*>): Boolean
    fun stop()
    fun start()
}