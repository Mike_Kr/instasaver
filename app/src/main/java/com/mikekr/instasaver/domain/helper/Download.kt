package com.mikekr.instasaver.domain.helper

import com.mikekr.instasaver.data.model.download.Response
import io.reactivex.Observable

interface Download {
    fun getResponseObject(clipboardUrl: String) : Observable<Response>
}