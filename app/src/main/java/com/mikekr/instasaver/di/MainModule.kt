package com.mikekr.instasaver.di

import com.mikekr.instasaver.data.service.ClipboardApiImpl
import com.mikekr.instasaver.domain.service.ClipboardApi
import com.mikekr.instasaver.presentation.gallery.GalleryContract
import com.mikekr.instasaver.presentation.gallery.GalleryPresenter
import com.mikekr.instasaver.presentation.main.DownloadContract
import com.mikekr.instasaver.presentation.main.DownloadPresenter
import com.mikekr.instasaver.presentation.main.ZoomAvatarContract
import com.mikekr.instasaver.presentation.main.ZoomAvatarPresenter
import com.mikekr.instasaver.presentation.router.Router
import com.mikekr.instasaver.presentation.router.RouterImpl
import org.koin.dsl.module

val mainModule = module {
    factory { DownloadPresenter(get(), get()) as DownloadContract.Presenter }
    factory { GalleryPresenter(get()) as GalleryContract.Presenter }
    factory { ZoomAvatarPresenter(get()) as ZoomAvatarContract.Presenter}
    single { ClipboardApiImpl(get()) as ClipboardApi }
    single { RouterImpl(get()) as Router }
}