package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Owner_ (

    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("profile_pic_url")
    @Expose
    var profilePicUrl: String? = null,
    @SerializedName("username")
    @Expose
    var username: String? = null,
    @SerializedName("blocked_by_viewer")
    @Expose
    var blockedByViewer: Boolean? = null,
    @SerializedName("followed_by_viewer")
    @Expose
    var followedByViewer: Boolean? = null,
    @SerializedName("full_name")
    @Expose
    var fullName: String? = null,
    @SerializedName("has_blocked_viewer")
    @Expose
    var hasBlockedViewer: Boolean? = null,
    @SerializedName("is_private")
    @Expose
    var isPrivate: Boolean? = null,
    @SerializedName("is_unpublished")
    @Expose
    var isUnpublished: Boolean? = null,
    @SerializedName("is_verified")
    @Expose
    var isVerified: Boolean? = null,
    @SerializedName("requested_by_viewer")
    @Expose
    var requestedByViewer: Boolean? = null

)