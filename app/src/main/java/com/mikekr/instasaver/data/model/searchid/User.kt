package com.mikekr.instasaver.data.model.searchid

import com.google.gson.annotations.SerializedName

data class User(

    @SerializedName("is_private")
    val isPrivate: Boolean? = null,

    @SerializedName("full_name")
    val fullName: String? = null,

    @SerializedName("reel_auto_archive")
    val reelAutoArchive: String? = null,

    @SerializedName("mutual_followers_count")
    val mutualFollowersCount: Int? = null,

    @SerializedName("pk")
    val pk: String? = null,

    @SerializedName("has_anonymous_profile_picture")
    val hasAnonymousProfilePicture: Boolean? = null,

    @SerializedName("profile_pic_url")
    val profilePicUrl: String? = null,

    @SerializedName("is_verified")
    val isVerified: Boolean? = null,

    @SerializedName("follower_count")
    val followerCount: Int? = null,

    @SerializedName("byline")
    val byline: String? = null,

    @SerializedName("unseen_count")
    val unseenCount: Int? = null,

    @SerializedName("username")
    val username: String? = null,

    @SerializedName("profile_pic_id")
    val profilePicId: String? = null
)