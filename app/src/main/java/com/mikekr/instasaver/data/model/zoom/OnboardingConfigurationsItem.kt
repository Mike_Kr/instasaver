package com.mikekr.instasaver.data.model.zoom

import com.google.gson.annotations.SerializedName

data class OnboardingConfigurationsItem(

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("vertical_id")
    val verticalId: Long? = null,

    @SerializedName("value_props")
    val valueProps: List<ValuePropsItem?>? = null,

    @SerializedName("type")
    val type: Int? = null
)