package com.mikekr.instasaver.data.helper

import com.mikekr.instasaver.data.model.download.Response
import com.mikekr.instasaver.data.model.searchid.SearchUsers
import com.mikekr.instasaver.data.model.zoom.ResponseUser
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NetworkInterface {
    @GET("?__a=1")
    fun getDownloadResponse(): Observable<Response>

    @GET("web/search/topsearch/")
    fun getIdUser(@Query("query") username: String): Observable<SearchUsers>

    @GET("{id}/info/")
    fun getUser(@Path("id") idUser: String): Observable<ResponseUser>
}