package com.mikekr.instasaver.data.helper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.mikekr.instasaver.R
import com.mikekr.instasaver.domain.helper.Notification
import com.mikekr.instasaver.presentation.main.MainActivity

const val NOTIFICATION_ID = 11
const val CHANNEL_ID = "Service_channel"

class NotificationHelper : Notification {

    override fun show(context: Context) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.channel_name)
            val notificationChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            notificationChannel.description = context.getString(R.string.channel_description)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val resultIntent = Intent(context, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentIntent(resultPendingIntent)
                .setOngoing(true)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher_round))
                .setSmallIcon(R.drawable.ic_small_notification)
                .setContentTitle(context.getString(R.string.notification_title))
                .setStyle(NotificationCompat.BigTextStyle().bigText(context.getString(R.string.notification_content)))
                .setContentText(context.getString(R.string.notification_content))
        } else {
            NotificationCompat.Builder(context)
                .setContentIntent(resultPendingIntent)
                .setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher_round))
                .setContentTitle(context.getString(R.string.notification_title))
                .setStyle(NotificationCompat.BigTextStyle().bigText(context.getString(R.string.notification_content)))
                .setContentText(context.getString(R.string.notification_content))
        }
        notificationManager.notify(NOTIFICATION_ID, builder.build())
    }

    override fun hide(context: Context) {
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.cancel(NOTIFICATION_ID)
    }
}