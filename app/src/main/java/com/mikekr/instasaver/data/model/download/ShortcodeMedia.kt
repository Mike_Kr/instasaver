package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ShortcodeMedia (

    @SerializedName("__typename")
    @Expose
    var typename: String? = null,
    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("shortcode")
    @Expose
    var shortcode: String? = null,
    @SerializedName("gating_info")
    @Expose
    var gatingInfo: Any? = null,
    @SerializedName("media_preview")
    @Expose
    var mediaPreview: Any? = null,
    @SerializedName("display_url")
    @Expose
    var displayUrl: String? = null,
    @SerializedName("video_url")
    @Expose
    var videoUrl: String? = null,
    @SerializedName("is_video")
    @Expose
    var isVideo: Boolean? = null,
    @SerializedName("should_log_client_event")
    @Expose
    var shouldLogClientEvent: Boolean? = null,
    @SerializedName("tracking_token")
    @Expose
    var trackingToken: String? = null,
    @SerializedName("caption_is_edited")
    @Expose
    var captionIsEdited: Boolean? = null,
    @SerializedName("comments_disabled")
    @Expose
    var commentsDisabled: Boolean? = null,
    @SerializedName("taken_at_timestamp")
    @Expose
    var takenAtTimestamp: Int? = null,
    @SerializedName("viewer_has_liked")
    @Expose
    var viewerHasLiked: Boolean? = null,
    @SerializedName("viewer_has_saved")
    @Expose
    var viewerHasSaved: Boolean? = null,
    @SerializedName("viewer_has_saved_to_collection")
    @Expose
    var viewerHasSavedToCollection: Boolean? = null,
    @SerializedName("owner")
    @Expose
    var owner: Owner_? = null,
    @SerializedName("is_ad")
    @Expose
    var isAd: Boolean? = null,
    @SerializedName("edge_sidecar_to_children")
    @Expose
    var edgeSidecarToChildren: EdgeSidecarToChildren? = null

)