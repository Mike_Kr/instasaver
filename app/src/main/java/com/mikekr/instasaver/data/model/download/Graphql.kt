package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Graphql (

    @SerializedName("shortcode_media")
    @Expose
    var shortcodeMedia: ShortcodeMedia? = null

)