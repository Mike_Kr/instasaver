package com.mikekr.instasaver.data.model.zoom

import com.google.gson.annotations.SerializedName

data class User(

    @SerializedName("is_private")
    val isPrivate: Boolean? = null,

    @SerializedName("reel_auto_archive")
    val reelAutoArchive: String? = null,

    @SerializedName("auto_expand_chaining")
    val autoExpandChaining: Boolean? = null,

    @SerializedName("media_count")
    val mediaCount: Int? = null,

    @SerializedName("has_highlight_reels")
    val hasHighlightReels: Boolean? = null,

    @SerializedName("hd_profile_pic_url_info")
    val hdProfilePicUrlInfo: HdProfilePicUrlInfo? = null,

    @SerializedName("follower_count")
    val followerCount: Int? = null,

    @SerializedName("usertags_count")
    val usertagsCount: Int? = null,

    @SerializedName("external_url")
    val externalUrl: String? = null,

    @SerializedName("following_tag_count")
    val followingTagCount: Int? = null,

    @SerializedName("profile_pic_url")
    val profilePicUrl: String? = null,

    @SerializedName("profile_pic_id")
    val profilePicId: String? = null,

    @SerializedName("biography")
    val biography: String? = null,

    @SerializedName("has_anonymous_profile_picture")
    val hasAnonymousProfilePicture: Boolean? = null,

    @SerializedName("is_verified")
    val isVerified: Boolean? = null,

    @SerializedName("external_lynx_url")
    val externalLynxUrl: String? = null,

    @SerializedName("hd_profile_pic_versions")
    val hdProfilePicVersions: List<HdProfilePicVersionsItem?>? = null,

    @SerializedName("full_name")
    val fullName: String? = null,

    @SerializedName("onboarding_configurations")
    val onboardingConfigurations: List<OnboardingConfigurationsItem?>? = null,

    @SerializedName("highlight_reshare_disabled")
    val highlightReshareDisabled: Boolean? = null,

    @SerializedName("following_count")
    val followingCount: Int? = null,

    @SerializedName("is_potential_business")
    val isPotentialBusiness: Boolean? = null,

    @SerializedName("is_interest_account")
    val isInterestAccount: Boolean? = null,

    @SerializedName("pk")
    val pk: Long? = null,

    @SerializedName("username")
    val username: String? = null
)