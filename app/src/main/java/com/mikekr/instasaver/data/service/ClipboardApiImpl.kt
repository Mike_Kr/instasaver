package com.mikekr.instasaver.data.service

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Build
import com.mikekr.instasaver.domain.service.ClipboardApi

class ClipboardApiImpl(private val context: Context) : ClipboardApi {

    override var serviceIsRunning = false

    override fun start() {
        serviceIsRunning = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(Intent(context, ClipboardService::class.java))
        }
        context.startService(Intent(context, ClipboardService::class.java))
    }

    override fun stop() {
        context.stopService(Intent(context, ClipboardService::class.java))
    }

    override fun handleService() {
        if (serviceIsRunning) {
            start()
            serviceIsRunning = false
        } else {
            start()
        }
    }

    override fun isClipboardServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}