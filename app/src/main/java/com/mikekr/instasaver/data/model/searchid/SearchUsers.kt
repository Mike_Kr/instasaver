package com.mikekr.instasaver.data.model.searchid

import com.google.gson.annotations.SerializedName

data class SearchUsers(

    @SerializedName("places")
    val places: List<Any?>? = null,

    @SerializedName("hashtags")
    val hashtags: List<Any?>? = null,

    @SerializedName("clear_client_cache")
    val clearClientCache: Boolean? = null,

    @SerializedName("rank_token")
    val rankToken: String? = null,

    @SerializedName("has_more")
    val hasMore: Boolean? = null,

    @SerializedName("users")
    val users: List<UsersItem?>? = null,

    @SerializedName("status")
    val status: String? = null
)