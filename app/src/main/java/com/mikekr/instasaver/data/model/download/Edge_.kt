package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Edge_ (

    @SerializedName("node")
    @Expose
    var node: Node_? = null

)