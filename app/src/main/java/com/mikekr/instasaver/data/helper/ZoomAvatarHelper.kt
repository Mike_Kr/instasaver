package com.mikekr.instasaver.data.helper

import android.util.Log
import com.google.gson.GsonBuilder
import com.mikekr.instasaver.data.model.searchid.SearchUsers
import com.mikekr.instasaver.data.model.zoom.HdProfilePicUrlInfo
import com.mikekr.instasaver.data.model.zoom.ResponseUser
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.function.Predicate

class ZoomAvatarHelper {

    fun getIdUser(username: String, listener: Listener) {

        var actualUsername: String? = ""

        getResponseByUsername(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                if (response.users?.isNotEmpty() == true) {
                    response.users?.find {
                        it?.user?.username?.toLowerCase().equals(username.toLowerCase())
                    }.also { actualUsername = it?.user?.fullName }
                        ?.user?.pk?.let {
                        getUser(it, actualUsername, listener)
                    } ?: error("No found user")
                } else {
                    error("No found user")
                }
            }, {
                listener.errorAvatar()
            })
    }

    private fun getUser(id: String, username: String?, listener: Listener) {
        getResponseUser(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ responseUser ->
                responseUser.user?.hdProfilePicUrlInfo?.let { listener.responseAvatar(it, username) }
            }, {
                //it.message
                listener.errorAvatar()
            })
    }

    private fun getResponseByUsername(str: String): Observable<SearchUsers> {
        val userName: String? = try {
            if (str.contains("instagram.com")) {
                val substringPosition = str.indexOf("instagram.com/") + 14
                var tempStr = str.substring(substringPosition)
                if (tempStr.contains("/")) {
                    tempStr = tempStr.substring(0, tempStr.indexOf("/", 0, true))
                }
                if (tempStr.contains("?")) {
                    tempStr = tempStr.substring(0, tempStr.indexOf("?", 0, true))
                }
                tempStr
            } else {
                str
            }
        } catch (e: StringIndexOutOfBoundsException) {
            null
        }

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://www.instagram.com/").build()
            .create(NetworkInterface::class.java).getIdUser(userName ?: "")
    }

    private fun getResponseUser(id: String): Observable<ResponseUser> {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://i.instagram.com/api/v1/users/").build()
            .create(NetworkInterface::class.java).getUser(id)

    }


    interface Listener {
        fun responseAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?)
        fun errorAvatar()
    }
}