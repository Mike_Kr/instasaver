package com.mikekr.instasaver.data.model.searchid

import com.google.gson.annotations.SerializedName

data class UsersItem(

    @SerializedName("position")
    val position: Int? = null,

    @SerializedName("user")
    val user: User? = null
)