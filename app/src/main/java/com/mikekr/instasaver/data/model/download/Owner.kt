package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Owner (

    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("profile_pic_url")
    @Expose
    var profilePicUrl: String? = null,
    @SerializedName("username")
    @Expose
    var username: String? = null

)