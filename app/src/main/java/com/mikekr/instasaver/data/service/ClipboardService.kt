package com.mikekr.instasaver.data.service

import android.app.DownloadManager
import android.app.Service
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.os.IBinder
import com.mikekr.instasaver.data.helper.DownloadHelper
import com.mikekr.instasaver.data.helper.NotificationHelper
import com.mikekr.instasaver.data.model.download.Response
import com.mikekr.instasaver.domain.service.Clipboard
import com.mikekr.instasaver.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class ClipboardService : Clipboard, ClipboardManager.OnPrimaryClipChangedListener, Service() {

    private lateinit var clipboardManager: ClipboardManager
    private lateinit var downloadManager: DownloadManager
    private var disposable: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        clipboardManager.addPrimaryClipChangedListener(this)
        NotificationHelper().show(this)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onPrimaryClipChanged() {
        if (clipboardManager.hasPrimaryClip()
            && clipboardManager.primaryClip != null
            && clipboardManager.primaryClip?.getItemAt(0) != null
            && clipboardManager.primaryClip?.getItemAt(0)?.text != null
        ) {
            var clipboard = clipboardManager.primaryClip?.getItemAt(0)?.text
            if (clipboard?.startsWith(URL_START) == true || clipboard?.startsWith(URL_IGTV_START) == true) {
                if (clipboard.contains(URL_DELIMITERS)) {
                    clipboard = clipboard.split(URL_DELIMITERS).first()
                }
                if (clipboard.last() != '/') {
                    clipboard = "$clipboard/"
                }
                download(clipboard.toString())
            }
        }
    }

    override fun download(url: String) {
        disposable = DownloadHelper().getResponseObject(url)
            .subscribeOn(Schedulers.io())
            .onErrorReturnItem(Response())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { response ->
                if (response.graphql != null) {
                    val userName = response.graphql?.shortcodeMedia?.owner?.username ?: ""

                    when (response.graphql?.shortcodeMedia?.edgeSidecarToChildren?.edges == null) {
                        true -> {
                            if (response.graphql?.shortcodeMedia?.isVideo == true) {
                                addDownloadTask(response.graphql?.shortcodeMedia?.videoUrl!!, userName)
                            } else {
                                addDownloadTask(response.graphql?.shortcodeMedia?.displayUrl!!, userName)
                            }
                        }
                        else -> {
                            response.graphql?.shortcodeMedia?.edgeSidecarToChildren?.edges?.forEach {
                                if (it.node?.isVideo == true) {
                                    addDownloadTask(it.node?.videoUrl!!, userName)
                                } else {
                                    addDownloadTask(it.node?.displayUrl!!, userName)
                                }
                            }
                        }
                    }
                }
            }
    }

    override fun addDownloadTask(url: String, name: String) {
        checkFolder()
        val request = DownloadManager.Request(Uri.parse(url))
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())

        if (url.contains(VIDEO_EXTENSION)) {
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_PICTURES,
                "$DOWNLOAD_DIR${File.separator}$name-$timeStamp$VIDEO_EXTENSION"
            )
        } else if (url.contains(IMAGE_EXTENSION)) {
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_PICTURES,
                "$DOWNLOAD_DIR${File.separator}$name-$timeStamp$IMAGE_EXTENSION"
            )
        }
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        downloadManager.enqueue(request)
    }

    override fun onDestroy() {
        clipboardManager.removePrimaryClipChangedListener(this)
        disposable?.dispose()
        NotificationHelper().hide(this)
        super.onDestroy()
    }
}