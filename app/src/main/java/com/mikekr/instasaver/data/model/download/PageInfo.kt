package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PageInfo(

    @SerializedName("has_next_page")
    @Expose
    var hasNextPage: Boolean? = null,
    @SerializedName("end_cursor")
    @Expose
    var endCursor: Any? = null

)