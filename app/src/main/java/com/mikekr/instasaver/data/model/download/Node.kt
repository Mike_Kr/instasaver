package com.mikekr.instasaver.data.model.download

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Node(

    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("text")
    @Expose
    var text: String? = null,
    @SerializedName("created_at")
    @Expose
    var createdAt: Int? = null,
    @SerializedName("owner")
    @Expose
    var owner: Owner? = null

)