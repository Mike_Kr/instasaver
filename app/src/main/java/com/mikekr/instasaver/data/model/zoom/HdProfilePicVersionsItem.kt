package com.mikekr.instasaver.data.model.zoom

import com.google.gson.annotations.SerializedName

data class HdProfilePicVersionsItem(

    @SerializedName("width")
    val width: Int? = null,

    @SerializedName("url")
    val url: String? = null,

    @SerializedName("height")
    val height: Int? = null
)