package com.mikekr.instasaver.utils

const val URL_START = "https://www.instagram.com/p/"
const val URL_IGTV_START = "https://www.instagram.com/tv"
const val URL_DELIMITERS = "?"
const val DOWNLOAD_DIR = "InstaSaver"
const val DELETE_DIALOG = "Delete Dialog"
const val IMAGE_TYPE = "image/*"
const val IMAGE_ID = "Image id"
const val IMAGE_EXTENSION = ".jpg"
const val VIDEO_EXTENSION = ".mp4"
const val PACKAGE_INSTAGRAM = "com.instagram.android"