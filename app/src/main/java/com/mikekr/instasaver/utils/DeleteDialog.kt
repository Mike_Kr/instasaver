package com.mikekr.instasaver.utils

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.mikekr.instasaver.R

class DeleteDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val id: Int = arguments?.getInt(IMAGE_ID) ?: 0

        val dialogListener = activity

        return if (dialogListener is DeleteDialogListener) {
            AlertDialog.Builder(dialogListener)
                .setMessage(R.string.dialog_message_delete)
                .setPositiveButton(R.string.dialog_btn_delete) { _, _ -> dialogListener.onDeleteDialogPositiveClick(id)}
                .setNegativeButton(R.string.dialog_btn_cancel) { _, _ -> dismiss()}
                .setCancelable(false)
                .create()
        } else {
            super.onCreateDialog(savedInstanceState)
        }

    }

    interface DeleteDialogListener {
        fun onDeleteDialogPositiveClick(id: Int)
    }

    companion object {
        fun newInstance(id: Int) = DeleteDialog().apply { arguments = Bundle(1).apply { putInt(IMAGE_ID, id) } }
    }

}