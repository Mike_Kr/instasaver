package com.mikekr.instasaver.utils

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.*
import android.view.animation.Interpolator
import android.widget.RelativeLayout
import com.mikekr.instasaver.R

class WaveLayout
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    RelativeLayout(context, attrs, defStyleAttr) {

    private var animatorSet: AnimatorSet? = null
    private var centerX: Float = 0.toFloat()
    private var centerY: Float = 0.toFloat()
    private var count: Int = 0
    private var color: Int = 0
    private var duration: Int = 0
    private var interpolator: Int = 0
    private val paint = Paint()
    private var radius: Float = 0.toFloat()
    private var repeat: Int = 0
    private var startFromScratch: Boolean = false
    private val views = ArrayList<View>()
    private var isStartedTemp: Boolean = false

    private val isStarted: Boolean
        @Synchronized get() = animatorSet != null && isStartedTemp

    private val animatorListener = object : Animator.AnimatorListener {

        override fun onAnimationStart(animation: Animator?) {
            isStartedTemp = true
        }

        override fun onAnimationEnd(animation: Animator?) {
            isStartedTemp = false
        }

        override fun onAnimationCancel(animation: Animator?) {
            isStartedTemp = false
        }

        override fun onAnimationRepeat(animation: Animator?) {}
    }

    init {
        val attr = context.theme.obtainStyledAttributes(attrs, R.styleable.Wave, 0, 0)

        count = DEFAULT_COUNT
        duration = DEFAULT_DURATION
        repeat = DEFAULT_REPEAT
        startFromScratch = DEFAULT_START_FROM_SCRATCH
        color = DEFAULT_COLOR
        interpolator = DEFAULT_INTERPOLATOR

        try {
            count = attr.getInteger(R.styleable.Wave_wave_count, DEFAULT_COUNT)
            duration = attr.getInteger(R.styleable.Wave_wave_duration, DEFAULT_DURATION)
            repeat = attr.getInteger(R.styleable.Wave_wave_repeat, DEFAULT_REPEAT)
            startFromScratch = attr.getBoolean(R.styleable.Wave_wave_startFromScratch, DEFAULT_START_FROM_SCRATCH)
            color = attr.getColor(R.styleable.Wave_wave_color, DEFAULT_COLOR)
            interpolator = attr.getInteger(R.styleable.Wave_wave_interpolator, DEFAULT_INTERPOLATOR)
        } finally {
            attr.recycle()
        }

        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        paint.shader = LinearGradient(
            0f,
            300f,
            300f,
            height.toFloat(),
            Color.parseColor("#7D5ACE"),
            Color.parseColor("#A438A2"),
            Shader.TileMode.MIRROR
        )

        // create Views
        build()
    }

    /**
     * Start pulse animation.
     */
    @Synchronized
    fun start() {
        if (animatorSet == null || isStartedTemp) {
            return
        }

        animatorSet?.start()

        if(!startFromScratch) {
            val animators = animatorSet!!.childAnimations
            for(animator in animators) {
                val objectAnimator = animator as ObjectAnimator

                val delay = objectAnimator.startDelay
                objectAnimator.startDelay = 0
                objectAnimator.currentPlayTime = duration - delay
            }
        }
    }

    /**
     * Stop pulse animation.
     */
    @Synchronized
    fun stop() {
        if (animatorSet == null || !isStartedTemp) {
            return
        }
        animatorSet?.end()
    }

    /**
     * Stop pulse animation.
     */
    @Synchronized
    fun pause() {
        if (animatorSet == null || !isStartedTemp) {
            return
        }
        reset()
        animatorSet?.end()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        val height = MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom

        centerX = width * 0.5f
        centerY = height * 0.5f
        radius = Math.min(width, height) * 0.5f

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    /**
     * Remove all views and animators.
     */
    private fun clear() {
        // remove animators
        stop()

        // remove old views
        for (view in views) {
            removeView(view)
        }
        views.clear()
    }

    /**
     * Build pulse views and animators.
     */
    private fun build() {
        // create views and animators
        val layoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT
        )

        val repeatCount = if (repeat == INFINITE) ObjectAnimator.INFINITE else repeat

        val animators = ArrayList<Animator>()
        for (index in 0 until count) {
            // setup view
            val pulseView = PulseView(context)
            pulseView.scaleX = 0f
            pulseView.scaleY = 0f
            pulseView.alpha = 1f

            addView(pulseView, index, layoutParams)
            views.add(pulseView)

            val delay = (index * duration / count).toLong()

            // setup animators
            val scaleXAnimator = ObjectAnimator.ofFloat(pulseView, "ScaleX", 0f, 1f)
            scaleXAnimator.repeatCount = repeatCount
            scaleXAnimator.repeatMode = ObjectAnimator.RESTART
            scaleXAnimator.startDelay = delay
            animators.add(scaleXAnimator)

            val scaleYAnimator = ObjectAnimator.ofFloat(pulseView, "ScaleY", 0f, 1f)
            scaleYAnimator.repeatCount = repeatCount
            scaleYAnimator.repeatMode = ObjectAnimator.RESTART
            scaleYAnimator.startDelay = delay
            animators.add(scaleYAnimator)

            val alphaAnimator = ObjectAnimator.ofFloat(pulseView, "Alpha", 1f, 0f)
            alphaAnimator.repeatCount = repeatCount
            alphaAnimator.repeatMode = ObjectAnimator.RESTART
            alphaAnimator.startDelay = delay
            animators.add(alphaAnimator)
        }

        animatorSet = AnimatorSet()
        animatorSet?.playTogether(animators)
        animatorSet?.interpolator = createInterpolator(interpolator)
        animatorSet?.duration = duration.toLong()
        animatorSet?.addListener(animatorListener)
    }

    /**
     * Reset views and animations.
     */
    private fun reset() {
        val isStarted = isStarted

        clear()
        build()

        if (isStarted) {
            start()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        animatorSet?.cancel()
        animatorSet = null
    }

    private inner class PulseView(context: Context) : View(context) {

        override fun onDraw(canvas: Canvas?) {
            canvas?.drawCircle(centerX, centerY, radius, paint)
        }

    }


    companion object {
        const val INFINITE = 0

        private const val INTERP_LINEAR = 0
        private const val INTERP_ACCELERATE = 1
        private const val INTERP_DECELERATE = 2
        private const val INTERP_ACCELERATE_DECELERATE = 3

        private const val DEFAULT_COUNT = 4
        private val DEFAULT_COLOR = Color.rgb(0, 116, 193)
        private const val DEFAULT_DURATION = 7000
        private const val DEFAULT_REPEAT = INFINITE
        private const val DEFAULT_START_FROM_SCRATCH = true
        private const val DEFAULT_INTERPOLATOR = INTERP_LINEAR

        private fun createInterpolator(type: Int): Interpolator {
            return when (type) {
                INTERP_ACCELERATE -> AccelerateInterpolator()
                INTERP_DECELERATE -> DecelerateInterpolator()
                INTERP_ACCELERATE_DECELERATE -> AccelerateDecelerateInterpolator()
                else -> LinearInterpolator()
            }
        }
    }

}