package com.mikekr.instasaver.presentation.base

interface BasePresenter<T> {

    fun start()

    var view: T
}