package com.mikekr.instasaver.presentation.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class SampleSlide : Fragment() {

    private val ARG_LAYOUT_RES_ID = "layoutResId"
    private var layoutResId: Int = 0

    fun newInstance(layoutResId: Int) : SampleSlide {
        val sampleSlide = SampleSlide()

        val args = Bundle()
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId)
        sampleSlide.arguments = args

        return sampleSlide
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(ARG_LAYOUT_RES_ID))
            layoutResId = arguments!!.getInt(ARG_LAYOUT_RES_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResId, container, false)

    }
}