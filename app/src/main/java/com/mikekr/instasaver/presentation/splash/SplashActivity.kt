package com.mikekr.instasaver.presentation.splash

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import com.mikekr.instasaver.presentation.intro.IntroActivity
import com.mikekr.instasaver.presentation.main.MainActivity

const val FIRST_START = "isFirstStart"

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val t = Thread(Runnable {
            val getPrefs = PreferenceManager.getDefaultSharedPreferences(this)

            val isFirstStart = getPrefs.getBoolean(FIRST_START, true)

            if (isFirstStart) {
                startActivity(Intent(this, IntroActivity::class.java))

                val e = getPrefs.edit()
                e.putBoolean(FIRST_START, false)
                e.apply()
            } else {
                startActivity(Intent(this, MainActivity::class.java))
            }
            finish()
        })
        t.start()
    }
}
