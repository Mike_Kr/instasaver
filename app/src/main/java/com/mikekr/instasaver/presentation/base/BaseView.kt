package com.mikekr.instasaver.presentation.base

interface BaseView<out T : BasePresenter<*>> {
    val presenter: T
}