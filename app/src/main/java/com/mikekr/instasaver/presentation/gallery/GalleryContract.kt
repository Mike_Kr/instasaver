package com.mikekr.instasaver.presentation.gallery

import com.mikekr.instasaver.presentation.base.BasePresenter
import com.mikekr.instasaver.presentation.base.BaseView

interface GalleryContract {
    interface View: BaseView<Presenter> {
        fun printPhoto(path: String)
        fun initViewPager(list: MutableList<String>)
        fun refreshListPager(list: MutableList<String>)
        fun showCannotOpenGalleryToastEmpty()
        fun showCannotOpenGalleryToastPermission()
    }

    interface Presenter: BasePresenter<View> {
        fun shareFile(id: Int)
        fun printPhoto(id: Int)
        fun deleteFile(id: Int)
        fun refreshList()
        fun playVideo(path: String)
    }
}