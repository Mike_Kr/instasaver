package com.mikekr.instasaver.presentation.main

import com.mikekr.instasaver.data.helper.ZoomAvatarHelper
import com.mikekr.instasaver.data.model.zoom.HdProfilePicUrlInfo
import com.mikekr.instasaver.presentation.router.Router

class ZoomAvatarPresenter(private val router: Router) : ZoomAvatarContract.Presenter, ZoomAvatarHelper.Listener {

    override lateinit var view: ZoomAvatarContract.View

    override fun responseAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?) {
        view.showAvatar(hdProfilePicUrlInfo, username)
    }

    override fun errorAvatar() {
        view.errorLoadAvatar()
    }

    override fun searchByUsername(username: String) {
       ZoomAvatarHelper().getIdUser(username, this)
    }

    override fun saveAvatar() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun start() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}