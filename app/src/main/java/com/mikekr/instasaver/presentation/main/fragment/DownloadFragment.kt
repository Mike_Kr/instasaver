package com.mikekr.instasaver.presentation.main.fragment

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.mikekr.instasaver.R
import com.mikekr.instasaver.presentation.intro.IntroActivity
import com.mikekr.instasaver.presentation.main.DownloadContract
import com.mikekr.instasaver.utils.toast
import kotlinx.android.synthetic.main.fragment_download.*
import org.koin.android.ext.android.inject
import permissions.dispatcher.*

@RuntimePermissions
class DownloadFragment : Fragment(), DownloadContract.View, CompoundButton.OnCheckedChangeListener, View.OnClickListener
{
    override val presenter by inject<DownloadContract.Presenter>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_download, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.view = this
        switchService.setOnCheckedChangeListener(this)
        tvOpenInstagram.setOnClickListener(this)
        tvOpenGallery.setOnClickListener(this)
        btnHelp.setOnClickListener(this)
    }

    override fun showStart() {
        wave.start()
        iv.setImageResource(R.drawable.ic_instagram)
    }

    override fun hideStart() {
        wave.pause()
        iv.setImageResource(R.drawable.ic_instagram_bw)
    }

    override fun onStart() {
        super.onStart()
        if (presenter.isRunningService()) {
            switchService.isChecked = true
            showStart()
        }
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun startService() {
        presenter.startService()
        tvOpenInstagram.isEnabled = true
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun openGallery() {
        presenter.openGallery()
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showExplainingRationaleDialog(request: PermissionRequest) {
        showRationaleDialog(request)
    }

    private fun showRationaleDialog(request: PermissionRequest) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.dialog_title)
            .setMessage(R.string.dialog_message)
            .setPositiveButton(R.string.dialog_ok) {_, _ -> request.proceed()}
            .setCancelable(false)
            .show()
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showNeverAskForStorage() {
        showNeverAskDialog()
        switchService.isChecked = false
    }

    private fun showNeverAskDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.dialog_permission_never_ask_again_title)
            .setMessage(R.string.dialog_permission_never_ask_again_message)
            .setPositiveButton(R.string.dialog_settings) {_, _ -> startActivity(Intent().apply {
                action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                data = Uri.fromParts("package", context?.packageName, null)
            })}
            .show()
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showToastPermission() {
        hideStart()
        switchService.isChecked = false
        activity?.toast(getString(R.string.toast_need_permission))
    }

    private fun stopService() {
        presenter.stopService()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            if (!presenter.isRunningService()) {
                startServiceWithPermissionCheck()
            } else {
                tvOpenInstagram.isEnabled = true
            }
        } else {
            stopService()
            tvOpenInstagram.isEnabled = false
        }
    }

    override fun onClick(v: View?) {
       when (v) {
           tvOpenInstagram -> presenter.openInstagram()
           tvOpenGallery -> openGalleryWithPermissionCheck()
           btnHelp -> startActivity(Intent(activity, IntroActivity::class.java))
       }
    }
}
