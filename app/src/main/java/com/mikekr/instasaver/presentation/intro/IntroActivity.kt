package com.mikekr.instasaver.presentation.intro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.github.paolorotolo.appintro.AppIntro
import com.mikekr.instasaver.R
import com.mikekr.instasaver.presentation.main.MainActivity

class IntroActivity : AppIntro() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSkipText(getString(R.string.app_intro_skip))
        setDoneText(getString(R.string.app_intro_done))
        addSlide(SampleSlide().newInstance(R.layout.intro1))
        addSlide(SampleSlide().newInstance(R.layout.intro2))
        addSlide(SampleSlide().newInstance(R.layout.intro3))
        addSlide(SampleSlide().newInstance(R.layout.intro4))
        addSlide(SampleSlide().newInstance(R.layout.intro5))
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        finish()
        loadMainActivity()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        finish()
        loadMainActivity()
    }

    private fun loadMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }


}
