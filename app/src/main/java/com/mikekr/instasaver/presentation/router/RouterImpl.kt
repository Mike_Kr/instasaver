package com.mikekr.instasaver.presentation.router

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.StrictMode
import android.widget.Toast
import com.mikekr.instasaver.R
import com.mikekr.instasaver.presentation.gallery.GalleryActivity
import com.mikekr.instasaver.utils.IMAGE_TYPE
import com.mikekr.instasaver.utils.PACKAGE_INSTAGRAM
import java.io.File
import java.lang.NullPointerException


class RouterImpl(private val context: Context) : Router {

    override fun launchInstagram() {
        val intent = context.packageManager.getLaunchIntentForPackage(PACKAGE_INSTAGRAM)
        try {
            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(context, context.getString(R.string.toast_no_instagram_app), Toast.LENGTH_SHORT).show()
        } catch (ex: NullPointerException) {
            Toast.makeText(context, context.getString(R.string.toast_no_instagram_app), Toast.LENGTH_SHORT).show()
        }
    }

    override fun openGallery() {
        val intent = Intent(context, GalleryActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    override fun sharePhoto(path: String) {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        val uriToImage = Uri.fromFile(File(path))
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = IMAGE_TYPE
        shareIntent.putExtra(Intent.EXTRA_STREAM, uriToImage)

        context.startActivity(
            Intent.createChooser(shareIntent, context.getString(R.string.share_photo_text)).addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
            )
        )
    }

    override fun playVideo(path: String) {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        val intent = Intent(Intent.ACTION_VIEW, Uri.fromFile(File(path)))
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.setDataAndType(Uri.fromFile(File(path)), "video/mp4")
        context.startActivity(intent)
    }
}