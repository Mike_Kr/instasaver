package com.mikekr.instasaver.presentation.router

interface Router {
    fun launchInstagram()
    fun openGallery()
    fun sharePhoto(path: String)
    fun playVideo(path: String)
}