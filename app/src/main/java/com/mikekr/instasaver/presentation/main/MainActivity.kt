package com.mikekr.instasaver.presentation.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mikekr.instasaver.R
import com.mikekr.instasaver.presentation.main.fragment.DownloadFragment
import com.mikekr.instasaver.presentation.main.fragment.ZoomAvatarFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.nav_download -> {
                fm.beginTransaction().hide(activeFragment).show(downloadFragment).commit()
                activeFragment = downloadFragment
                true
            }
            R.id.nav_zoom_avatar -> {
                fm.beginTransaction().hide(activeFragment).show(zoomAvatarFragment).commit()
                activeFragment = zoomAvatarFragment
                true
            }
            else -> false
        }
    }

    private val downloadFragment = DownloadFragment()
    private val zoomAvatarFragment = ZoomAvatarFragment()
    private lateinit var activeFragment: Fragment
    private val fm = supportFragmentManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activeFragment = downloadFragment
        fm.beginTransaction().add(R.id.mainContainer, downloadFragment, "downloadFragment").commit()
        fm.beginTransaction().add(R.id.mainContainer, zoomAvatarFragment, "zoomAvatarFragment").hide(zoomAvatarFragment).commit()
        bottomNavigationViewMorph.setOnNavigationItemSelectedListener(this)
    }
}
