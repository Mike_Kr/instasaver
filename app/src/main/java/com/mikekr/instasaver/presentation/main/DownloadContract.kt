package com.mikekr.instasaver.presentation.main

import com.mikekr.instasaver.presentation.base.BasePresenter
import com.mikekr.instasaver.presentation.base.BaseView

interface DownloadContract {

    interface View : BaseView<Presenter> {
        fun showStart()
        fun hideStart()
    }

    interface Presenter : BasePresenter<View> {
        fun startService()
        fun stopService()
        fun isRunningService() : Boolean
        fun openInstagram()
        fun openGallery()
    }


}