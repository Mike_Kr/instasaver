package com.mikekr.instasaver.presentation.main

import com.mikekr.instasaver.data.model.zoom.HdProfilePicUrlInfo
import com.mikekr.instasaver.presentation.base.BasePresenter
import com.mikekr.instasaver.presentation.base.BaseView

interface ZoomAvatarContract {
    interface View : BaseView<Presenter> {
        fun showLoading()
        fun hideLoading()
        fun showAvatar(hdProfilePicUrlInfo: HdProfilePicUrlInfo, username: String?)
        fun errorLoadAvatar()

    }

    interface Presenter : BasePresenter<View> {
        fun searchByUsername(username: String)
        fun saveAvatar()
    }

}