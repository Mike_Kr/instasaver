package com.mikekr.instasaver.presentation.gallery

import android.Manifest
import android.animation.ObjectAnimator
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.print.PrintHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.mikekr.instasaver.R
import com.mikekr.instasaver.utils.*
import kotlinx.android.synthetic.main.activity_gallery.*
import kotlinx.android.synthetic.main.switches_bottom_gallery.*
import kotlinx.android.synthetic.main.toolbar_gallery.*
import org.koin.android.ext.android.inject
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class GalleryActivity : AppCompatActivity(), View.OnClickListener, GalleryContract.View, GalleryAdapter.ClickListener,
    DeleteDialog.DeleteDialogListener {

    override val presenter by inject<GalleryContract.Presenter>()
    private var galleryAdapter: GalleryAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)
        setSupportActionBar(toolbarGallery)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setDisplayShowTitleEnabled(false)
        }

        presenter.view = this
        initialization()
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun initialization() {
        presenter.start()
        includeToolbar.visibility = View.VISIBLE
        includeLayoutBottom.visibility = View.VISIBLE
        btnDelete.setOnClickListener(this)
        ifLolliPop { btnPrint?.setOnClickListener(this) }
        btnShare.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.refreshList()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showToastPermission() {
        toast(getString(R.string.toast_need_permission))
        finish()
    }

    override fun onClickViewPager() {
        if (includeToolbar.visibility == View.VISIBLE) {
            showView(false)
        } else {
            showView(true)
        }
    }

    private fun showView(isShow: Boolean) {
        if (isShow) {
            ObjectAnimator.ofFloat(includeToolbar, "translationY", 0f)
                .setDuration(300)
                .start()
            includeToolbar.visibility = View.VISIBLE
            includeLayoutBottom.visibility = View.VISIBLE
        } else {
            ObjectAnimator.ofFloat(includeToolbar, "translationY", -includeToolbar.height.toFloat())
                .setDuration(300)
                .start()
            includeToolbar.visibility = View.INVISIBLE
            includeLayoutBottom.visibility = View.INVISIBLE
        }
    }

    override fun onClickPlay(path: String) {
        presenter.playVideo(path)
    }

    override fun onLastPageDelete() {
        onBackPressed()
    }

    override fun printPhoto(path: String) {
        val photoPrinter = PrintHelper(this)
        photoPrinter.scaleMode = PrintHelper.SCALE_MODE_FIT
        Glide.with(this)
            .asBitmap()
            .load(path)
            .into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    photoPrinter.printBitmap(path, resource)
                }
            })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun initViewPager(list: MutableList<String>) {
        if (list.isEmpty()) {
            onBackPressed()
            showCannotOpenGalleryToastEmpty()
        } else {
            galleryAdapter = GalleryAdapter(this, list, this)
            viewPager.adapter = galleryAdapter
            viewPager.setPageTransformer(true, DepthPageTransformer())
            viewPager.setOnClickListener(this)
        }
    }

    override fun refreshListPager(list: MutableList<String>) {
        galleryAdapter?.refresh(list)
    }

    override fun showCannotOpenGalleryToastEmpty() {
        toast(getString(R.string.empty_gallery))
    }

    override fun showCannotOpenGalleryToastPermission() {
        toast(getString(R.string.cannot_open_gallery_permission))
    }

    override fun onClick(v: View?) {
        when (v) {
            btnDelete -> {
                DeleteDialog.newInstance(viewPager.currentItem).show(supportFragmentManager, DELETE_DIALOG)
            }
            btnShare -> {
                presenter.shareFile(viewPager.currentItem)
            }
            btnPrint -> {
                presenter.printPhoto(viewPager.currentItem)
            }
        }

    }

    override fun onDeleteDialogPositiveClick(id: Int) {
        presenter.deleteFile(id)
        galleryAdapter?.delete(id)
    }

}
