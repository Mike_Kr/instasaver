package com.mikekr.instasaver.presentation.gallery

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.mikekr.instasaver.R
import com.mikekr.instasaver.utils.VIDEO_EXTENSION
import kotlinx.android.synthetic.main.item_pager.view.*

class GalleryAdapter(context: Context) : PagerAdapter() {
    private lateinit var context: Context
    private lateinit var pathsList: MutableList<String>
    private var clickListener: ClickListener? = null
    private val inflater = LayoutInflater.from(context)

    constructor(context: Context, images: List<String>, listener: ClickListener) : this(context) {
        this.context = context
        this.pathsList = images.toMutableList()
        clickListener = listener
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
       return pathsList.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = inflater.inflate(R.layout.item_pager, container, false)
        Glide.with(context)
            .load(pathsList[position])
            .into(itemView.ivPhoto)
        if (pathsList[position].contains(VIDEO_EXTENSION)) {
            itemView.ivPhoto.isZoomable = false
            itemView.btnPlay.visibility = View.VISIBLE
        } else {
            itemView.ivPhoto.isZoomable = true
            itemView.btnPlay.visibility = View.INVISIBLE
        }
        itemView.ivPhoto.setOnClickListener{
            clickListener?.onClickViewPager()
        }
        itemView.btnPlay.setOnClickListener {
            clickListener?.onClickPlay(pathsList[position])
        }
        itemView.setOnClickListener{
            clickListener?.onClickViewPager()
        }
        container.addView(itemView)

        return itemView
    }

    fun refresh(list: List<String>) {
        if (list != pathsList) {
            pathsList = list.toMutableList()
            notifyDataSetChanged()
        }
    }

    fun delete(id: Int) {
        pathsList.removeAt(id)
        if (pathsList.isEmpty()) run {
            clickListener?.onLastPageDelete()
        }
        notifyDataSetChanged()
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    interface ClickListener{
        fun onClickViewPager()
        fun onClickPlay(path: String)
        fun onLastPageDelete()
    }

}