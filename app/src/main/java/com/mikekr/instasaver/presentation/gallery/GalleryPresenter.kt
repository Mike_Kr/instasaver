package com.mikekr.instasaver.presentation.gallery

import com.mikekr.instasaver.presentation.router.Router
import com.mikekr.instasaver.utils.getSortedByNameListFiles
import com.mikekr.instasaver.utils.deleteFile

class GalleryPresenter(private val router: Router): GalleryContract.Presenter {

    override lateinit var view: GalleryContract.View

    override fun shareFile(id: Int) {
        router.sharePhoto(getSortedByNameListFiles()[id])
    }

    override fun printPhoto(id: Int) {
        view.printPhoto(getSortedByNameListFiles()[id])
    }

    override fun deleteFile(id: Int) {
        deleteFile(getSortedByNameListFiles()[id])
    }

    override fun refreshList() {
        view.refreshListPager(getSortedByNameListFiles())
    }

    override fun playVideo(path: String) {
        router.playVideo(path)
    }

    override fun start() {
        view.initViewPager(getSortedByNameListFiles())
    }

}