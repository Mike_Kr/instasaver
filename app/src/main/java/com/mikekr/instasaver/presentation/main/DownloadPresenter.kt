package com.mikekr.instasaver.presentation.main

import com.mikekr.instasaver.data.service.ClipboardService
import com.mikekr.instasaver.domain.service.ClipboardApi
import com.mikekr.instasaver.presentation.router.Router

class DownloadPresenter(private val serviceApi: ClipboardApi, private val router: Router) : DownloadContract.Presenter {

    override lateinit var view: DownloadContract.View

    override fun start() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun startService() {
        serviceApi.start()
        view.showStart()
    }

    override fun stopService() {
        serviceApi.stop()
        view.hideStart()
    }

    override fun openInstagram() {
        router.launchInstagram()
    }

    override fun openGallery() {
        router.openGallery()
    }

    override fun isRunningService(): Boolean {
        return serviceApi.isClipboardServiceRunning(ClipboardService::class.java)
    }
}